import os
import copy
import numpy as np
import cupy as cp
import json
from chainercv.chainer_experimental.datasets.sliceable import GetterDataset
from chainercv.utils import read_image
import chainer
from chainer import cuda
# from chainercv.links.model.ssd import multibox_loss
from chainercv.links import SSD300
from chainer.datasets import TransformDataset
from chainercv import transforms
from chainercv.links.model.ssd import random_crop_with_bbox_constraints, random_distort, resize_with_random_interpolation
from imgaug import augmenters as iaa
from refinedet.refinedet.links.model.ssd_vgg16 import RefineDet320
from refinedet.refinedet.links.model.multibox_loss import multibox_loss
sometimes = lambda aug: iaa.Sometimes(0.5, aug)
seq = iaa.Sequential([
    iaa.GaussianBlur(sigma=(0, 1.0)),
    sometimes(iaa.OneOf([
        iaa.Dropout((0.01, 0.1), per_channel=0.5),
        iaa.CoarseDropout((0.03, 0.15), size_percent=(0.02, 0.05), per_channel=0.2),
        ])),
])

class BboxDataset(GetterDataset):
    def __init__(self, img_dir, annotation_dir, categories, img_ext='.jpg', annotation_ext='.json'):
        super(BboxDataset, self).__init__()

        self.names = [i.split('.')[0] for i in os.listdir(img_dir)]
        self.img_dir = img_dir
        self.annotation_dir = annotation_dir
        self.categories = categories
        self.img_ext = img_ext
        self.annotation_ext = annotation_ext
        self.add_getter('img', self.get_image)
        self.add_getter(('bbox', 'label'), self.get_annotation)

    def __len__(self):
        return len(self.names)

    def get_image(self, i):
        name = self.names[i]
        img_path = os.path.join(self.img_dir, name+self.img_ext)
        img = read_image(img_path, color=True)

        return img

    def get_annotation(self, i):
        name = self.names[i]
        annotation_path = os.path.join(self.annotation_dir, name+self.annotation_ext)
        with open(annotation_path) as f:
            annotation = json.load(f)
        bbox = []
        label = []

        for l in annotation['labels']:
            if l['category'] in self.categories:
                bb = l['box2d']
                bbox.append([bb['y1'], bb['x1'], bb['y2'], bb['x2']])
                label.append(self.categories.index(l['category']))
        bbox = np.array(bbox).astype(np.float32)
        label = np.array(label).astype(np.int32)

        return bbox, label

class MultiboxTrainChain(chainer.Chain):
    def __init__(self, model, alpha=1, k=3):
        super(MultiboxTrainChain, self).__init__()
        with self.init_scope():
            self.model = model
        self.alpha = alpha
        self.k = k

    def __call__(self, imgs, gt_mb_locs, gt_mb_labels):
        mb_locs, mb_confs = self.model(imgs)
        loc_loss, conf_loss = multibox_loss(
            mb_locs, mb_confs, gt_mb_locs, gt_mb_labels, self.k)
        loss = loc_loss * self.alpha + conf_loss

        chainer.reporter.report(
            {'loss': loss, 'loss/loc': loc_loss, 'loss/conf': conf_loss},
            self)

        return loss

class RefineDetTrainChain(chainer.Chain):

    def __init__(self, model, k=3):
        super(RefineDetTrainChain, self).__init__()
        with self.init_scope():
            self.model = model
        self.k = k

    def __call__(self, imgs, gt_mb_locs, gt_mb_labels):
        arm_locs, arm_confs, odm_locs, odm_confs = self.model(imgs)

        gt_objectness_label = gt_mb_labels.copy()
        gt_objectness_label[gt_objectness_label > 0] = 1

        arm_loc_loss, arm_conf_loss = multibox_loss(
            arm_locs, arm_confs, gt_mb_locs.copy(), gt_objectness_label, self.k,
            binarize=True)

        xp = cuda.get_array_module(arm_confs)
        objectness = xp.zeros_like(arm_confs.array)
        objectness[arm_confs.array >= 0.01] = 1
        odm_loc_loss, odm_conf_loss = multibox_loss(
            odm_locs, odm_confs, gt_mb_locs, gt_mb_labels, self.k,
            arm_confs=objectness, arm_locs=arm_locs)
        loss = arm_loc_loss + arm_conf_loss + odm_loc_loss + odm_conf_loss

        chainer.reporter.report(
            {'loss': loss,
             'arm_loss/loc': arm_loc_loss,
             'arm_loss/conf': arm_conf_loss,
             'odm_loss/loc': odm_loc_loss,
             'odm_loss/conf': odm_conf_loss}, self)

        return loss

class Transform():
    def __init__(self, coder, size, mean):
        # to send cpu, make a copy
        self.coder = copy.copy(coder)
        self.coder.to_cpu()

        self.size = size
        self.mean = mean

    def __call__(self, in_data):
        img, bbox, label = in_data

        # 1. Color augmentation
        img = random_distort(img)
        img = seq.augment_image(img.transpose((1, 2, 0))).transpose((2, 0, 1))

        # 2. Random expansion
        if np.random.randint(2):
            img, param = transforms.random_expand(
                img, fill=self.mean, return_param=True)
            bbox = transforms.translate_bbox(
                bbox, y_offset=param['y_offset'], x_offset=param['x_offset'])

        # 3. Random cropping
        img, param = random_crop_with_bbox_constraints(
            img, bbox, return_param=True)
        bbox, param = transforms.crop_bbox(
            bbox, y_slice=param['y_slice'], x_slice=param['x_slice'],
            allow_outside_center=False, return_param=True)
        label = label[param['index']]

        # 4. Resizing with random interpolatation
        _, H, W = img.shape
        img = resize_with_random_interpolation(img, (self.size, self.size))
        bbox = transforms.resize_bbox(bbox, (H, W), (self.size, self.size))

        # 5. Random horizontal flipping
        img, params = transforms.random_flip(
            img, x_random=True, return_param=True)
        bbox = transforms.flip_bbox(
            bbox, (self.size, self.size), x_flip=params['x_flip'])

        # Preparation for SSD network
        img -= self.mean
        mb_loc, mb_label = self.coder.encode(bbox, label)

        return img, mb_loc, mb_label

def generate_mini_train_and_validation_data(n_train=2000, n_valid=200):
    import shutil
    _train_images_path = 'data/dtc_train_images'
    _train_annotations_path = 'data/dtc_train_annotations'
    train_images_files = os.listdir(_train_images_path)
    if not os.path.exists('mini_train_images'):
        os.mkdir('mini_train_images')
    if not os.path.exists('mini_train_annotations'):
        os.mkdir('mini_train_annotations')
    if not os.path.exists('mini_val_images'):
        os.mkdir('mini_val_images')
    if not os.path.exists('mini_val_annotations'):
        os.mkdir('mini_val_annotations')
    count = 0
    for train_images_file in train_images_files:
        annotation_file_name =  os.path.splitext(train_images_file)[0]+'.json'
        if count < n_train:
            shutil.copy(os.path.join(_train_images_path, train_images_file), os.path.join('mini_train_images', train_images_file))
            shutil.copy(os.path.join(_train_annotations_path, annotation_file_name), os.path.join('mini_train_annotations', annotation_file_name))
        else:
            shutil.copy(os.path.join(_train_images_path, train_images_file), os.path.join('mini_val_images', train_images_file))
            shutil.copy(os.path.join(_train_annotations_path, annotation_file_name), os.path.join('mini_val_annotations', annotation_file_name))
        count += 1
        if count == n_train + n_valid:
            break

def train(train_chain, image_dir='data/dtc_train_images', annotation_dir='data/dtc_train_annotations',
          batchsize=22, num_epochs=30, gpu_id=0):
    from chainer.optimizer_hooks import WeightDecay
    from chainercv.links.model.ssd import GradientScaling
    from chainer import training
    from chainer.training import extensions, triggers
    from chainercv.extensions import DetectionVOCEvaluator

    train_data = BboxDataset(image_dir, annotation_dir, eval_categories)
    transformed_train_data = TransformDataset(train_data, Transform(model.coder, model.insize, model.mean))
    train_iter = chainer.iterators.SerialIterator(transformed_train_data, batchsize)
    train_chain.model.to_gpu()
    optimizer = chainer.optimizers.MomentumSGD(lr=0.001)
    optimizer.setup(train_chain)

    for param in train_chain.params():
        if param.name == 'b':
            param.update_rule.add_hook(GradientScaling(2))
        else:
            param.update_rule.add_hook(WeightDecay(0.0001))

    updater = training.updaters.StandardUpdater(train_iter, optimizer, device=gpu_id)
    trainer = training.Trainer(updater, (num_epochs, 'epoch'), 'results')

    log_interval = 1, 'epoch'
    trainer.extend(extensions.LogReport(trigger=log_interval))
    trainer.extend(extensions.observe_lr(), trigger=log_interval)
    trainer.extend(extensions.ExponentialShift('lr', 0.2), trigger=(20, 'epoch'))
    trainer.extend(extensions.PrintReport(['epoch', 'iteration', 'lr','main/loss', 'main/loss/loc', 'main/loss/conf']), trigger=log_interval)
    trainer.extend(extensions.ProgressBar(update_interval=10))
    trainer.extend(extensions.snapshot_object(train_chain.model, 'model_epoch_{.updater.epoch}'), trigger=(10, 'epoch'))
    trainer.run()

def test(train_chain, image_dir='data/dtc_test_images', annotation_dir='',
         model_file=os.path.join('results', 'model_epoch_{}'.format(30))):
    test_data = BboxDataset(image_dir, annotation_dir, eval_categories)
    chainer.serializers.load_npz(model_file, train_chain.model)
    prediction = {}
    ground_truth = {}
    for i, name in enumerate(test_data.names):
        print(name)
        img_name = name + '.jpg'
        if annotation_dir == '':
            img = test_data.get_image(i)
        else:
            img, bbox, label = test_data[i]
            ground_truth[img_name]={}
            for bb, label in zip(bbox, label):
                y1, x1, y2, x2 = list(bb)
                label_name = eval_categories[label]
                if label_name not in ground_truth[img_name]:
                    ground_truth[img_name][eval_categories[label]]=[]
                ground_truth[img_name][eval_categories[label]].append([int(x1), int(y1), int(x2), int(y2)])
        bboxes, labels, scores = train_chain.model.predict([img])
        prediction[img_name] = {}
        for bb, label in zip(bboxes[0], labels[0]):
            y1, x1, y2, x2 = list(bb)
            label_name = eval_categories[label]
            if label_name not in prediction[img_name]:
                prediction[img_name][eval_categories[label]] = []
            prediction[img_name][eval_categories[label]].append([float(x1), float(y1), float(x2), float(y2)])

    with open('prediction.json', 'w') as f:
        json.dump(prediction, f)
    with open('ground_truth.json', 'w') as f:
        json.dump(ground_truth, f)

if __name__ == '__main__':
    import argparse
    usage = 'Usage: python {} INPUT_FILE [--output <output>] [--help]'.format(__file__)
    parser = argparse.ArgumentParser(description='This script is for Object detection challenge.',
                                     usage=usage)
    parser.add_argument('-m', '--mode', action='store', type=str,
                        choices=['train', 'val', 'test'], default='train', help='Execution mode.')
    parser.add_argument('-b', '--batchsize', action='store', type=int,
                        default=22, help='Batch size')
    parser.add_argument('--train_img_dir', action='store', type=str,
                        default='data/dtc_train_images', help='Directory of trainnig images')
    parser.add_argument('--train_anno_dir', action='store', type=str,
                        default='data/dtc_train_annotations', help='Directory of trainnig annotations')
    parser.add_argument('--test_img_dir', action='store', type=str,
                        default='data/dtc_test_images', help='Directory of test images')
    args = parser.parse_args()
    category_names = ('Motorbike', 'Bicycle', 'Car', 'Signal', 'SVehicle', 'Truck', 'Bus', 'Pedestrian', 'Signs', 'Train')
    eval_categories = ('Bicycle', 'Car', 'Signs', 'Truck', 'Signal', 'Pedestrian')
    # model = SSD300(n_fg_class=len(eval_categories), pretrained_model = 'imagenet')
    model = RefineDet320(n_fg_class=len(eval_categories), pretrained_model='imagenet')
    model.nms_thresh = 0.3
    model.score_thresh = 0.5
    if isinstance(model, RefineDet320):
        train_chain = RefineDetTrainChain(model)
    else:
        train_chain = MultiboxTrainChain(model)
    if args.mode == 'train':
        train(train_chain, image_dir=args.train_img_dir, annotation_dir=args.train_anno_dir, batchsize=args.batchsize)
    elif args.mode == 'val':
        # generate_mini_train_and_validation_data(n_train=2000, n_valid=200)
        train(train_chain, 'mini_train_images', 'mini_train_annotations')
        test(train_chain, 'mini_val_images', 'mini_val_annotations')
    elif args.mode == 'test':
        test(train_chain, image_dir=args.test_img_dir)