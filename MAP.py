# -*- coding: utf-8 -*-

import json
import argparse

def MMAP(true, pred, threshold):
    eval_files = set(true).intersection(set(pred))
    score = 0
    count = 0
    for eval_file in eval_files:
        score += MAP(pred[eval_file], true[eval_file], threshold)
        count+=1
    return score/len(true)

def MAP(pr, gt, threshold):
    num_true_categories = len(gt)
    aps = 0
    for c, pred_bbs in pr.items():
        ap = 0
        detected = 0
        if c not in gt:
            num_true = 0
        else:
            num_true = len(gt[c])
        num_pred = len(pred_bbs)
        
        if num_true>0 and num_pred>0:
            del_recall = 1/min(num_true, num_pred)
            for i,pred_bb in enumerate(pred_bbs):
                if len(gt[c])>0:
                    pred_true_dist = {j:compute_iou_bb(pred_bb, true_bb) for j,true_bb in enumerate(gt[c])}
                    nearest = max(pred_true_dist.items(), key = lambda x:x[1])
                    if nearest[1]>=threshold:
                        detected+=1
                        ap += del_recall*(detected/(i+1))
                        del gt[c][nearest[0]]
                else:
                    break
        aps+=ap
    return aps/num_true_categories

def compute_iou_bb(pred_bb, true_bb):
    pred_area = (pred_bb[2]-pred_bb[0])*(pred_bb[3]-pred_bb[1])
    true_area = (true_bb[2]-true_bb[0])*(true_bb[3]-true_bb[1])
    intersection_x = max(min(pred_bb[2],true_bb[2])-max(pred_bb[0],true_bb[0]),0)
    intersection_y = max(min(pred_bb[3],true_bb[3])-max(pred_bb[1],true_bb[1]),0)
    intersection_area = intersection_x*intersection_y
    union_area = pred_area+true_area-intersection_area
    if union_area>0:
        return intersection_area/union_area
    else:
        return 0

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--path_to_ground_truth', type = str, help = 'path to ground truth', nargs='?')
    parser.add_argument('-p', '--path_to_prediction', type = str, help = 'path to prediction', nargs='?')
    parser.add_argument('-t', '--threshold', type = float, help = 'threshold', default = 0.75, nargs='?')
    args = parser.parse_args()
    
    with open(args.path_to_ground_truth) as f:
        true = json.load(f)
    with open(args.path_to_prediction) as f:
        pred = json.load(f)
    
    score = MMAP(true, pred, args.threshold)
    print(score)